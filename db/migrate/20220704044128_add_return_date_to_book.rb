class AddReturnDateToBook < ActiveRecord::Migration[5.1]
  def change
    add_column :books, :return_date, :datetime
  end
end
