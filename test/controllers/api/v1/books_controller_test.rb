require 'test_helper'

class BooksControllerTest < ActionDispatch::IntegrationTest
  test "index action should be success" do
    get api_v1_books_url

    assert_response :success
  end

  test "create action should be success" do
    post api_v1_books_url,
      params: {
        title: "New book",
        author: "book author",
        description: "New book for library system",
        usage_time: 10
      }

    assert_response :success
  end

  test "show action should be success" do
    book = books(:one)
    get api_v1_book_url(book.id)

    assert_response :success
  end

  test "update action should be success" do
    book = books(:one)
    put api_v1_book_url(book.id),
      params: {
        id: book.id,
        title: "New book name",
        usage_time: 20
      }

    assert_response :success
  end

  test "delete action should be success" do
    book = books(:one)
    delete api_v1_book_url(book.id)

    assert_response :success
  end
end
