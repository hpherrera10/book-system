require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  test "index action should be success" do
    get api_v1_users_url

    assert_response :success
  end

  test "create action should be success" do
    post api_v1_users_url,
      params: {
        name: "New User",
        email: "new_eamil@test.cl",
      }

    assert_response :success
  end

  test "show action should be success" do
    user = users(:one)
    get api_v1_user_url(user.id)

    assert_response :success
  end

  test "update action should be success" do
    user = users(:one)
    put api_v1_user_url(user.id),
      params: {
        id: user.id,
        name: "new name"
      }

    assert_response :success
  end

  test "delete action should be success" do
    user = users(:one)
    delete api_v1_user_url(user.id)

    assert_response :success
  end
end
