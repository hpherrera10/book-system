# README

## Getting started

#### Pre requisites

- Ruby 2.6.5
- Rails 5.1
- PostgreSQL 12

```shell

brew install postgresql@12
brew services start postgresql@12

gem install bundler
bundle install
```

#### Database

```shell
rails db:create
rails db:migrate
```

#### Rails server

```shell
rails s -p 5000
```

#### Test

```shell
rails test
```
#### Api Swagger

```shell
rails s -p 5000
```

```shell
http://localhost:5000/api-docs/index.html
```

### Example Api Swagger

![image.png](./image.png)

## Create Book

![image-1.png](./image-1.png)

![image-2.png](./image-2.png)

![image-3.png](./image-3.png)

![image-4.png](./image-4.png)

![image-5.png](./image-5.png)
