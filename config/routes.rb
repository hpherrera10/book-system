# == Route Map
#
#                                                      Prefix Verb      URI Pattern                                            Controller#Action
#                                           api_v1_books_path	GET	      /api/v1/books(.:format)	                               api/v1/books#index
#                                                             POST	    /api/v1/books(.:format)	                               api/v1/books#create
#                                           api_v1_book_path	GET	      /api/v1/books/:id(.:format)	                           api/v1/books#show
#                                                             PATCH	    /api/v1/books/:id(.:format)	                           api/v1/books#update
#                                                             PUT	      /api/v1/books/:id(.:format)	                           api/v1/books#update
#                                                             DELETE	  /api/v1/books/:id(.:format)	                           api/v1/books#destroy
#                                           api_v1_users_path	GET	      /api/v1/users(.:format)	                               api/v1/users#index
#                                                             POST	    /api/v1/users(.:format)	                               api/v1/users#create
#                                           api_v1_user_path	GET	      /api/v1/users/:id(.:format)	                           api/v1/users#show
#                                                             PATCH	    /api/v1/users/:id(.:format)	                           api/v1/users#update
#                                                             PUT	      /api/v1/users/:id(.:format)	                           api/v1/users#update
#                                                             DELETE	  /api/v1/users/:id(.:format)	                           api/v1/users#destroy
#

Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  
  namespace :api do
    namespace :v1 do 
      resources :books, only: %i[index show create update destroy] do
        put :loan, on: :member
        put :return, on: :member
      end

      resources :users, only: %i[index show create update destroy]
    end
  end
end