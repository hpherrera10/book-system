class UserMailer < ApplicationMailer
  default from: 'notifications@example.com'

  def book_information_email
    @user = params[:user]
    @book = params[:book]
    mail(to: @user.email, subject: 'Libro prestado')
  end
end
