class Book < ApplicationRecord
  validates :title, presence: true
  validates :author, presence: true
  validates :description, presence: true
  validates :usage_time, presence: true

  def loan_update(user_id)
    update(user_id: user_id, return_date: Time.zone.today + usage_time)
  end

  def loan_return
    update(user_id: nil, return_date: nil)
  end
end
