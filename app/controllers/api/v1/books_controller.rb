# app/controllers/api/v1/books_controller.rb
class Api::V1::BooksController < ApplicationController
  before_action :set_book, only: %i[show update destroy loan return]
  before_action :set_user, only: :loan
  before_action :book_params, only: :create

  # GET /api/v1/books
  def index
    @books = Book.all
    render json: @books
  end

  # POST /api/v1/books
  def create
    if @book.save
      render json: @book, each_serializer: BooksSerializer
    else
      render json: @book.errors, status: :unprocessable_entity
    end
  end

  # GET /api/v1/books/:id
  def show
    if @book
      render json: @book, each_serializer: BooksSerializer
    else
      render json: 'El registro no existe'
    end
  end

  # PUT /api/v1/books/:id
  def update
    if @book.update(build_update_params)
      render json: @book, each_serializer: BooksSerializer
    else
      render json: @book.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/books/:id
  def destroy
    if @book.user_id.nil? && @book.destroy!
      render json: { message: 'Libro eliminado satisfactoriamente' }, status: :ok
    else
      render json: { message: 'El libro esta asignado' }, status: :ok
    end
  rescue ActiveRecord::RecordNotFound
    render json: 'El registro ya fue destruido'
  rescue ActiveRecord::RecordNotDestroyed
    render json: 'El registro no fue destruido'
  end

  # PUT /api/v1/books/:id/loan
  def loan
    if @book.loan_update(@user.id)
      UserMailer.with(user: @user, book: @book).book_information_email.deliver_later

      render json: @book, each_serializer: BooksSerializer
    else
      render json: @book.errors, status: :unprocessable_entity
    end
  end

  # PUT /api/v1/books/:id/return
  def return
    if @book.update(user_id: nil, return_date: nil)
      render json: @book, each_serializer: BooksSerializer
    else
      render json: @book.errors, status: :unprocessable_entity
    end
  end

  private

  def book_params
    @book = Book.new
    @book.title = params[:title]
    @book.author = params[:author]
    @book.description = params[:description]
    @book.usage_time = params[:usage_time]
  end

  def set_book
    @book = Book.find(params[:id])
  end

  def set_user
    @user = User.find(params[:user_id])
  end

  def build_update_params
    {
      title: params[:title].presence || @book.title,
      author: params[:author].presence || @book.author,
      description: params[:description].presence || @book.description,
      usage_time: params[:usage_time].presence || @book.usage_time
    }
  end
end
