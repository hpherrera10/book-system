# app/controllers/api/v1/users_controller.rb
class Api::V1::UsersController < ApplicationController
  before_action :set_user, only: %i[show update destroy]
  before_action :user_params, only: :create

  # GET /api/v1/users
  def index
    @users = User.all
    render json: @users
  end

  # POST /api/v1/users
  def create
    if @user.save
      render json: @user, each_serializer: UsersSerializer
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # GET /api/v1/users/:id
  def show
    if @user
      render json: @user, each_serializer: UsersSerializer
    else
      render json: {}, status: :not_found
    end
  end

  # PUT /api/v1/users/:id
  def update
    if @user.present?
      if @user.update(build_update_params)
        render json: @user, each_serializer: UsersSerializer
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    else
      render json: {}, status: :not_found
    end
  end

  # DELETE /api/v1/users/:id
  def destroy
    @user.destroy!
    render json: { message: 'Usuario eliminado satisfactoriamente' }, status: :ok
  rescue ActiveRecord::RecordNotFound
    render json: 'El registro ya fue destruido'
  rescue ActiveRecord::RecordNotDestroyed
    render json: 'El registro no fue destruido'
  end

  private

  def user_params
    @user = User.new
    @user.name = params[:name]
    @user.email = params[:email]
  end

  def set_user
    @user = User.find(params[:id])
  end

  def build_update_params
    {
      name: params[:name].presence || @user.email,
      email: params[:email].presence || @user.email
    }
  end
end
