class BooksSerializer < ActiveModel::Serializer
  attributes %i[id title description author image usage_time return_date]
end
